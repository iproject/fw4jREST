package com.cn.fw4j.util.oro;

import org.apache.oro.text.perl.Perl5Util;
import org.apache.oro.text.regex.MatchResult;
import org.apache.oro.text.regex.PatternMatcherInput;

public class Perl5 {
	/**
	 * 
	 * @param str 字符串
	 * @param regex 正则表达式
	 * @return 匹配返回 true 否则返回 false
	 */
	public static boolean match(String str,String regex){
			Perl5Util p = new Perl5Util();
			PatternMatcherInput input = new PatternMatcherInput(str);
			return p.match(regex, input);
	}
	
	public static String match2(String str,String regex){
		Perl5Util p = new Perl5Util();
		PatternMatcherInput input = new PatternMatcherInput(str);
		String results = null;
		if(p.match(regex, input)){
			MatchResult result = p.getMatch();
			results = result.toString();
		}
		return results;
	}
}













