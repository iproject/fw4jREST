package com.cn.fw4j.util;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ScanningFileHelper {
	private static Set<String> fileSet = new HashSet<String>();
	private final static Logger logger = LoggerFactory.getLogger(ScanningFileHelper.class);
	
	static{
		String path = Thread.currentThread().getContextClassLoader().getResource("/").getPath();
		String val = PropertiesHelper.get("fw4j.properties", "FW4J_DIR");
		if(val != null)
			path = System.getProperty(val);
		logger.debug(path);
		scanning(path);
	}
	
	public static Set<String> getFileSet() {
		return fileSet;
	}

	public static final void scanning(String pg) {
	        File file = new File(pg); 
	        File[] childFiles = file.listFiles();
	        for (File childFile : childFiles) { 
	            if (childFile.isDirectory()) { 
	                 scanning(childFile.getPath()); 
	            } else { 
	                String childFilePath = childFile.getPath(); 
	                fileSet.add(childFilePath);
	            }
	        } 
	}
}
