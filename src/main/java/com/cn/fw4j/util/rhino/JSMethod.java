package com.cn.fw4j.util.rhino;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Function;
import org.mozilla.javascript.Scriptable;

public class JSMethod {
	private Context cx;
	private String name;
	private Scriptable scope;
	
	public JSMethod(String name,Context cx,Scriptable scope) {
		this.name = name;
		this.scope = scope;
		this.cx = cx;
	}
	
	public String invoke(Object[] args){
		Object obj = scope.get(name, scope);
		if(obj instanceof Function){
			Function func = (Function)obj;
			Object result = func.call(cx, scope, scope, args);
			String val =  Context.toString(result);
			Context.exit();
			return val;
		}
		return null;
	}

	public String invoke(){
		return this.invoke(null);
	}
}
