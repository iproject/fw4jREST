package com.cn.fw4j.util;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class ParseClassPackageHelper {
	public static String getPackage(String path){
		try {
			FileInputStream fis = new FileInputStream(path);
			DataInputStream dis = new DataInputStream(new BufferedInputStream(fis));
			return analyze(dis);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	private static String analyze(DataInputStream input) throws IOException{
		int magic =  input.readInt();
		if (magic == 0xCAFEBABE){
			input.readShort();
			input.readShort();
			input.readShort();
			input.readByte();
			input.readShort();
			input.readByte();
			short size = input.readShort();
			byte[] buffer = new byte[size];
			input.readFully(buffer);
			return new String(buffer);
		}
		return null;
	}
}
