package com.cn.fw4j.util;

import java.lang.reflect.Method;


public final class ClassHelper{
	public static Method getMethodByName(Method[] ms,String name){
		for(Method m : ms){
			if(m.getName().equals(name)){
				return m;
			}
		}
		return null;
	}
	public static Class<?> loadClass(String name) throws ClassNotFoundException{
		return Thread.currentThread().getContextClassLoader().loadClass(name);
	}
}
