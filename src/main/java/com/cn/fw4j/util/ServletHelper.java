package com.cn.fw4j.util;


import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileItemFactory;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import com.cn.fw4j.util.exception.UpLoadException;


public class ServletHelper {
	

	private String filename = null;
	private String path = null;
	private int size = -1;
	//////////////////////////////////////////
	private String tempPath = null;
	private int sizeThreshold = -1;
	/**
	 * types:List<String>
	 * image/jpeg
	 */
	private List<String> types = null;
	
	public ServletHelper(){
		filename =  String.valueOf(System.currentTimeMillis());
	}
	
	public ServletHelper(String filename, String path) {
		super();
		this.filename = filename;
		this.path = path;
	}
	
	public ServletHelper(String filename, String path, int size) {
		super();
		this.filename = filename;
		this.path = path;
		this.size = size;
	}
	/**
	 * @param tempPath String
	 * @param filename String
	 * @param path String
	 * @param size	int
	 * @param types List image/jpeg,image/png
	 */
	public ServletHelper(String tempPath, String filename, 
				String path,int size,
				List<String> types) {
		super();
		this.filename = filename;
		this.path = path;
		this.types = types;
		this.size = size;
	}
	/**
	 * 
	 * @param tempPath String 
	 * @param filename String
	 * @param path String
	 * @param size int
	 * @param sizeThreshold int
	 * @param types List image/jpeg,image/png
	 */
	public ServletHelper(String tempPath, String filename, String path,
			int size, int sizeThreshold, List<String> types) {
		super();
		this.tempPath = tempPath;
		this.filename = filename;
		this.path = path;
		this.size = size;
		this.sizeThreshold = sizeThreshold;
		this.types = types;
	}
	public Map<String,String> formData(HttpServletRequest request) throws IOException, UpLoadException{
		DiskFileItemFactory diskFileItemFactory = new DiskFileItemFactory();
		if(tempPath != null){
			diskFileItemFactory.setRepository(new File(tempPath));
		}
		if(sizeThreshold > 0){
			diskFileItemFactory.setSizeThreshold(sizeThreshold);
		}
		FileItemFactory  fileItemFactory = diskFileItemFactory;
		ServletFileUpload upload = new ServletFileUpload(fileItemFactory);
		upload.setSizeMax(50*1024*1024);
		Map<String, List<FileItem>> list = null;
		try {
			list = upload.parseParameterMap(request);
			Map<String,String> map = new HashMap<String,String>();
			for(String key : list.keySet()){
				for(FileItem item :list.get(key)){
					saveFile(item,map);
				}
			}
			return map;
		} catch (FileUploadException e) {
			e.printStackTrace();
		}
		return null;
	}
	private void saveFile(FileItem item,Map<String,String> map) throws UpLoadException, IOException {
		boolean bool = true;
		if(item.isFormField()){
			map.put(item.getFieldName(), StreamHelper.getContent(item.getInputStream()));
		}else{
			if(this.size > 0 && this.size > item.getSize()){
				throw new UpLoadException("文件大小超过界限");
			}
			if(types != null){
				bool = false;
				for(String type : types){
					if(bool) break;
					if(item.getContentType().equals(type)){
						bool = true;
					}
				}
			}
			if(bool){
				String name = item.getName();
				String suffix = name.substring(name.lastIndexOf('.')-1).toLowerCase(); 
				StringBuilder sbpath = new StringBuilder(path);
				sbpath.append("/");
				sbpath.append(filename);
				sbpath.append(suffix);
		        try {
					item.write(new File(sbpath.toString()));
				} catch (Exception e) {
					e.printStackTrace();
				}
		        map.put(item.getFieldName(),filename+suffix);
			}
		}
	}
	public String getTempPath() {
		return tempPath;
	}
	public void setTempPath(String tempPath) {
		this.tempPath = tempPath;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public List<String> getTypes() {
		return types;
	}
	public void setTypes(List<String> types) {
		this.types = types;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public int getSizeThreshold() {
		return sizeThreshold;
	}
	public void setSizeThreshold(int sizeThreshold) {
		this.sizeThreshold = sizeThreshold;
	}
}








