package com.cn.fw4j.util.exception;

public class UpLoadException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public UpLoadException(String str){
		super(str);
	}
}
