package com.cn.fw4j.mvc.servlet.inter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface IServletController {
	/**
	 * 调用controller
	 * @param request
	 * @param response
	 * @param urlPath
	 */
	void controller(HttpServletRequest request,HttpServletResponse response, String urlPath);
}
