package com.cn.fw4j.mvc.servlet.inter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface IDispatcherHandle {
	 void get(HttpServletRequest request,HttpServletResponse response);
	 void post(HttpServletRequest request,HttpServletResponse response);
	 void put(HttpServletRequest request,HttpServletResponse response);
	 void delete(HttpServletRequest request,HttpServletResponse response);
}
