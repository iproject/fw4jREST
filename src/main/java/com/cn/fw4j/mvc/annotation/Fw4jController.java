package com.cn.fw4j.mvc.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Fw4jController {
	String value() default "";
	String template() default "com.cn.fw4j.mvc.page.impl.JspTemplate";
}
