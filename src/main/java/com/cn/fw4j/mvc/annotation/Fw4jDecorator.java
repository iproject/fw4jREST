package com.cn.fw4j.mvc.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Fw4jDecorator {
	enum Type{
		JAVA,
		GROOVY,
	}
    String value() default "";
    String method() default "";
	Type type() default Type.JAVA;
}
