package com.cn.fw4j.mvc.page.inter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cn.fw4j.util.request.parameter.OutParam;

public interface IPageTemplate {
	/**
	 * HTML模板
	 * @param path 路径
	 * @param outparam 页面参数
	 * @param request
	 * @param response
	 */
	void Template(String path,OutParam outparam,HttpServletRequest request,HttpServletResponse response);
}
