package com.cn.fw4j.container.inter;

public interface IController {
	
	/**
	 * 根据key获取一个GET
	 * @param key
	 * @return
	 */
	String gets(String key);
	/**
	 * 根据key获取一个POST
	 * @param key
	 * @return
	 */
	String posts(String key);
	/**
	 * 根据key获取一个PUT
	 * @param key
	 * @return
	 */
	String puts(String key);
	/**
	 * 根据key获取一个DELETE
	 * @param key
	 * @return
	 */
	String deletes(String key);
}
